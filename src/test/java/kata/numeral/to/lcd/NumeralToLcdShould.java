package kata.numeral.to.lcd;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class NumeralToLcdShould {

    @ParameterizedTest(name = "{0} should be displed on {1}")
    @CsvSource({
            "0,'" + " _ \n" +
                    "| |\n" +
                    "|_|\n" +
            "'",
            "1,'" + "   \n" +
                    "  |\n" +
                    "  |\n" +
            "'",
            "10,'" +    "    _ \n" +
                        "  || |\n" +
                        "  ||_|\n" +
            "'",
            "11,'" +    "      \n" +
                        "  |  |\n" +
                        "  |  |\n" +
            "'"
    })
    public void display_number_in_parameter_to_lcd_representation(int numberToDiplay, String lcdRepresentation){
        assertThat(NumeralToLcd.getInstance().convert(numberToDiplay)).as("should diplay the number")
                .isEqualTo(lcdRepresentation);
    }



    @Test
    public void displayTwo(){
        assertThat(NumeralToLcd.getInstance().convert(2)).as("should display two")
                .isEqualTo(
                        " _ \n" +
                        " _|\n" +
                        "|_ \n"
                );
    }

    @Test
    public void displayThree(){
        assertThat(NumeralToLcd.getInstance().convert(3)).as("should display three")
                .isEqualTo(
                        " _ \n" +
                        " _|\n" +
                        " _|\n"
                );
    }

    @Test
    public void displayFour(){
        assertThat(NumeralToLcd.getInstance().convert(4)).as("should display four")
                .isEqualTo(
                        "   \n" +
                        "|_|\n" +
                        "  |\n"
                );
    }

    @Test
    public void displayFive(){
        assertThat(NumeralToLcd.getInstance().convert(5)).as("should display five")
                .isEqualTo(
                        " _ \n" +
                        "|_ \n" +
                        " _|\n"
                );
    }
}