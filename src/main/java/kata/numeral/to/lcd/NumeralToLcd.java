package kata.numeral.to.lcd;

import static java.lang.String.format;

public class NumeralToLcd {

    public String convert(int numeralToDisplay) {
        int rigthPartOfNumberToDisplay = numeralToDisplay % 10;
        int leftPartOfNumberToDisplay = numeralToDisplay / 10;
        if(leftPartOfNumberToDisplay > 0){
            LCDPatterns rigthPattern = LCDPatterns.findPattern(rigthPartOfNumberToDisplay);
            LCDPatterns leftPattern = LCDPatterns.findPattern(leftPartOfNumberToDisplay);
            return rigthPattern.toString() + leftPattern.toString();
        }
        return  LCDPatterns.findPattern(numeralToDisplay).toString();
    }

    private enum LCDPatterns{
        NINE(
            9,
            new char[][]{
                    {' ','_',' '},
                    {'|','_','|'},
                    {' ','_','|'}
            }
        ),
        HEIGT(
            8,
            new char[][]{
                    {' ','_',' '},
                    {'|','_','|'},
                    {'|','_','|'}
            }
        ),
        SEVEN(
            7,
            new char[][]{
                    {' ','_',' '},
                    {' ',' ','|'},
                    {' ',' ','|'}
            }
        ),
        SIX(
            6,
            new char[][]{
                    {' ','_',' '},
                    {'|','_',' '},
                    {'|','_','|'}
            }
        ),
        FIVE(
            5,
            new char[][]{
                    {' ','_',' '},
                    {'|','_',' '},
                    {' ','_','|'}
            }
        ),
        FOUR(
            4,
            new char[][]{
                    {' ',' ',' '},
                    {'|','_','|'},
                    {' ',' ','|'}
            }
        ),
        THREE(
            3,
            new char[][]{
                    {' ','_',' '},
                    {' ','_','|'},
                    {' ','_','|'}
            }
        ),
        TWO(
            2,
            new char[][]{
                    {' ','_',' '},
                    {' ','_','|'},
                    {'|','_',' '}
            }
        ),
        ONE(
            1,
            new char[][]{
                    {' ',' ',' '},
                    {' ',' ','|'},
                    {' ',' ','|'}
            }
        ),
        ZERO(
            0,
            new char[][]{
                    {' ','_',' '},
                    {'|',' ','|'},
                    {'|','_','|'}
            }
        ),
        ;

        private final int numeralValue;

        private final char[][] pattern;

        LCDPatterns(int numeralValue, char[][] pattern) {
            this.numeralValue = numeralValue;
            this.pattern = pattern;
        }

        public static LCDPatterns findPattern(int numberToFind){
            for(LCDPatterns potentialPattern : values()){
                if(numberToFind == potentialPattern.numeralValue){
                    return potentialPattern;
                }
            }
            String message = format("The number on parameter don't have equivalent. Got \"%s\".", numberToFind);
            throw new IllegalArgumentException(message);
        }

        @Override
        public String toString() {
            StringBuilder lcdFormat = new StringBuilder();
            for(char[] lineCursor : this.pattern){
                for(char indexCursor : lineCursor){
                   lcdFormat.append(indexCursor);
                }
                lcdFormat.append('\n');
            }
            return lcdFormat.toString();
        }
    }

    private NumeralToLcd(){};

    public static NumeralToLcd getInstance() {
        return new NumeralToLcd();
    }
}
